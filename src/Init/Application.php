<?php

namespace JyHuaweiPP\Init;

use JyHuaweiPP\Kernel\Response;
use JyHuaweiPP\Huawei\Huawei;

/**
 * Class Application.
 */
class Application extends Huawei
{
  use Response;
  
  public function __construct(array $config = [])
  {
    parent::__construct($config);
  }
}

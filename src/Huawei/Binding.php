<?php

namespace JyHuaweiPP\Huawei;

use JyHuaweiPP\Kernel\Http;

trait Binding
{
    /**
     * 绑定号码
     * https://support.huaweicloud.com/api-PrivateNumber/privatenumber_02_0022.html
     *
     * @param array $params 参数
     * @return false|mixed
     */
    public function binding($params)
    {
        if (strpos($params['bindNum'], '+86') === false) {
            $params['bindNum'] = '+86' . $params['bindNum'];
        }
        if (strpos($params['virtualNum'], '+86') === false) {
            $params['virtualNum'] = '+86' . $params['virtualNum'];
        }
        // 非A用户呼叫X号码时，A看到的主显号码。由于运营商管控，当前平台要求该参数必须设置为0，否则呼叫会被运营商拦截。
        if (!isset($params['displayNumMode'])) {
            $params['displayNumMode'] = '0';
        }
        
        // 是否录音，true：录音，false：不录音，默认为true
        if (!isset($params['recordFlag'])) {
            $params['recordFlag'] = 'true';
        }
        
        // 时间单位。0：小时、1分钟
        $params['timeUnit'] = '1';
        
        // 绑定关系的有效时间，单位：由timeUnit控制。
        if (!isset($params['bindExpiredTime'])) {
            $params['bindExpiredTime'] = 30;
        }
        
        // 回呼记录有效时间，过期后系统会自动清除回呼记录，回呼将转接到callbackNum或播放callbackTone提示音
        if (!isset($params['callbackExpiredTime'])) {
            $params['callbackExpiredTime'] = min($params['bindExpiredTime'], 10080);
        }
        
        $info = $this->getCommonInfo('/rest/caas/extendnumber/v1.0');
        Http::setHeaders($info['headers']);
        $res = Http::httpPostJson($info['url'], $params);
        if (isset($res['resultcode']) && $res['resultcode'] == 0) {
            return $res;
        } else {
            return $this->setError($res);
        }
    }
    
    /**
     * 解除绑定
     * https://support.huaweicloud.com/api-PrivateNumber/privatenumber_02_0023.html
     *
     * @param array $params 参数
     * @return false|mixed
     */
    public function unBinding($params)
    {
        if (strpos($params['virtualNum'], '+86') === false) {
            $params['virtualNum'] = '+86' . $params['virtualNum'];
        }
        if (strpos($params['bindNum'], '+86') === false) {
            $params['bindNum'] = '+86' . $params['bindNum'];
        }
        $info = $this->getCommonInfo('/rest/caas/extendnumber/v1.0');
        Http::setHeaders($info['headers']);
        $res = Http::httpDelete($info['url'] . '?' . http_build_query($params));
        if (isset($res['resultcode']) && $res['resultcode'] == 0) {
            return $res;
        } else {
            return $this->setError($res);
        }
    }
    
    /**
     * 获取录音文件
     * https://support.huaweicloud.com/api-PrivateNumber/privatenumber_02_0007_3.html
     *
     * @param array $params 参数
     * @return false|mixed 成功直接返回二进制录音文件地址
     */
    public function getSoundRecording($params)
    {
        $info = $this->getCommonInfo('/rest/provision/voice/record/v1.0');
        Http::setHeaders($info['headers']);
        $res = Http::httpGet($info['url'] . '?' . http_build_query($params));
        if (Http::getStatusCode() == 200) {
            return $res;
        } else {
            return $this->setError($res);
        }
    }
}

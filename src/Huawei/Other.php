<?php

namespace JyHuaweiPP\Huawei;

use JyHuaweiPP\Kernel\Http;


/**
 * 其他
 */
trait Other
{
    /**
     * 绑定信息查询
     * https://support.huaweicloud.com/api-PrivateNumber/privatenumber_02_0024.html
     *
     * @param array $params 参数
     * @return false|mixed
     */
    public function queryBinding($params)
    {
        if (strpos($params['virtualNum'], '+86') === false) {
            $params['virtualNum'] = '+86' . $params['virtualNum'];
        }
        $info = $this->getCommonInfo('/rest/caas/extendnumber/v1.0');
        Http::setHeaders($info['headers']);
        $res = Http::httpGet($info['url'] . '?' . http_build_query($params));
        if (isset($res['resultcode']) && $res['resultcode'] == 0) {
            return $res;
        } else {
            return $this->setError($res);
        }
    }
    
    /**
     * 终止呼叫接口
     * https://support.huaweicloud.com/api-PrivateNumber/privatenumber_02_0009_3.html
     *
     * @remark 当前支持[AXN]
     * @param string $sessionid 通过“呼叫事件和话单通知接口”获取到呼叫的sessionId。
     * @return false|mixed
     */
    public function callStop($sessionid)
    {
        $params = [
            'sessionid' => $sessionid,
            'signal'    => 'call_stop',
        ];
        $info   = $this->getCommonInfo('/rest/httpsessions/callStop/v2.0');
        Http::setHeaders($info['headers']);
        $res = Http::httpPost($info['url'], $params);
        if (isset($res['resultcode']) && $res['resultcode'] == 0) {
            return $res;
        } else {
            return $this->setError($res);
        }
    }
}

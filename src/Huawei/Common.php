<?php

namespace JyHuaweiPP\Huawei;

/**
 * 全局
 */
trait Common
{
    protected function getCommonInfo($url)
    {
        return [
            'url'     => $this->domainUrl . $url,
            'headers' => $this->getHeaders($this->config['app_key'], $this->config['app_secret']),
        ];
    }
    
    protected function getHeaders($app_key, $app_secret)
    {
        return [
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json;charset=UTF-8',
            'Authorization' => 'AKSK realm="SDP",profile="UsernameToken",type="Appkey"',
            'X-AKSK'        => $this->buildAKSKHeader($app_key, $app_secret),
        ];
    }
    
    protected function buildAKSKHeader($app_key, $app_secret)
    {
        date_default_timezone_set("UTC");
        $Created = date('Y-m-d\TH:i:s\Z');                                                     // Created
        $nonce   = uniqid();                                                                   // Nonce
        $base64  = base64_encode(hash_hmac('sha256', ($nonce . $Created), $app_secret, TRUE)); // PasswordDigest
        return sprintf("UsernameToken Username=\"%s\",PasswordDigest=\"%s\",Nonce=\"%s\",Created=\"%s\"", $app_key, $base64, $nonce, $Created);
    }
}

<?php

namespace JyHuaweiPP\Huawei;

class Huawei
{
    use Common;
    use Binding;
    use Other;
    
    protected $domainUrl = 'https://rtcpns.cn-north-1.myhuaweicloud.com';
    protected $config = [];
    protected $error = null;
    protected $errorNum = [
        '1023006' => 'HTTP消息头未找到Authorization字段',
        '1023007' => 'Authorization字段中未找到realm属性',
        '1023008' => 'Authorization字段中未找到profile属性',
        '1023009' => 'Authorization中realm属性值应该为“SDP”',
        '1023010' => 'Authorization中profile属性值应该为“UsernameToken”',
        '1023011' => 'Authorization中type属性值应该为“Appkey”',
        '1023012' => 'Authorization字段中未找到type属性',
        '1023033' => 'HTTP头未找到X-AKSK字段',
        '1023034' => 'X-AKSK字段中未找到UserName属性',
        '1023035' => 'X-AKSK字段中未找到Nonce属性',
        '1023036' => 'X-AKSK字段中未找到Created属性',
        '1023037' => 'X-AKSK字段中未找到PasswordDigest属性',
        '1023038' => 'X-AKSK中没有携带UsernameToken',
        '1010010' => 'PasswordDigest校验失败(平台达到系统流控上限，请稍等一分钟后再试)',
        '1010013' => '时间超出限制',
        '1010002' => '无效请求。',
        '1010003' => '无效的app_key',
        '1010008' => 'app_key状态异常',
        '1010029' => '用户账号已冻结。',
        '1010040' => 'app_key没有调用本API的权限。',
        '1012001' => '资源未申请。',
        '1012007' => '记录不存在。',
        '1012008' => '号码资源不足。',
        '1012010' => '绑定关系已存在。',
        '1012012' => '应用未开启录音功能。',
        '1012102' => '号码状态异常。',
        '1016001' => '记录不存在',
        '1020166' => '对端app IP不在白名单列表中。',
        '1020167' => '没有空闲的分机号',
        '1020176' => '鉴权失败，稍后重试',
        '1020178' => '不允许显示真实主叫号码',
        '1010001' => '系统错误。',
        '1023001' => '内部错误。',
        '1023002' => '响应超时。',
    ];
    
    public function __construct($config = null)
    {
        $this->config = $config;
    }
    
    public function getError()
    {
        return $this->error;
    }
    
    private function setError($error)
    {
        if (isset($error['resultcode']) && isset($this->errorNum[$error['resultcode']])) {
            $error['msg'] = $this->errorNum[$error['resultcode']];
        }
        $this->error = $error;
        return false;
    }
}
